# -*- coding: utf-8 -*-"
#+STARTUP: overview indent
#+OPTIONS: html-link-use-abs-url:nil html-postamble:auto
#+OPTIONS: html-preamble:t html-scripts:t html-style:t
#+OPTIONS: html5-fancy:nil tex:t
#+HTML_DOCTYPE: xhtml-strict
#+HTML_CONTAINER: div
#+DESCRIPTION:
#+KEYWORDS:
#+HTML_LINK_HOME:
#+HTML_LINK_UP:
#+HTML_MATHJAX:
#+HTML_HEAD:
#+HTML_HEAD_EXTRA:
#+SUBTITLE:
#+INFOJS_OPT:
#+CREATOR: <a href="http://www.gnu.org/software/emacs/">Emacs</a> 25.2.2 (<a href="http://orgmode.org">Org</a> mode 9.0.1)
#+LATEX_HEADER:
#+EXPORT_EXCLUDE_TAGS: noexport
#+EXPORT_SELECT_TAGS: export
#+TAGS: noexport(n) deprecated(d)

* Planejamento                                                     :noexport:
** Modelo do cronograma em TEX                                      :ATTACH:
:PROPERTIES:
:Attachments: layout-cronograma.tex
:ID:       be43e1ff-0a91-4c3f-9e17-1fa62e6795ce
:END:

** Aulas (em ORG)

#+NAME: listagem_de_encontros
| Setor      | Descrição                                                        | Local | Projeto             |
|------------+------------------------------------------------------------------+-------+---------------------|
|            | Definições, requisitos e posicionamento da disciplina            |       | Definições iniciais |
| Léxica     | Introdução, Expressões Regulares, =flex=                           |       | Início E1 (flex)    |
| Léxica     | Autômato Finito Determinístico, Conv. AFND para AFD, =flex= (demo) |       |                     |
| Sintática  | Intro., Gramáticas Livres de Contexto, Transformações            |       |                     |
| Sintática  | Desc., com Retrocesso, Preditivo (Primeiro, Sequência)           |       |                     |
| Sintática  | Parser e tabela LL(1), Exercício LL(1), =bison=                    |       | Início E2 (bison)   |
| Sintática  | Ascendente, Parser LR(0), SLR(1) , =bison= (demo)                  |       |                     |
| Sintática  | Exercícios LR(0), SLR(1)                                         |       |                     |
| Sintática  | Parser LR(1), LALR(1)                                            |       |                     |
|            |                                                                  | TBD   | Avaliação E1, E2    |
| Sintática  | Exercícios LR(1), e LALR(1), AST e =bison= (ações)                 |       | Início E3 (Árvore)  |
| Semântica  | Esquemas S e L-Atribuídos                                        |       |                     |
| Semântica  | Implementação de Esquemas S e L-Atribuídos                       |       |                     |
|            | Revisão                                                          |       | Início E4 (Tipos)   |
|            | P1                                                               |       |                     |
| Código     | Declarações, Escopos e Atribuição                                |       |                     |
|            |                                                                  | TBD   | Avaliação E3        |
| Código     | Expressões Lógicas com atalho, Controle de Fluxo                 |       |                     |
| Código     | Endereçamento de Arranjos multidimensionais                      |       |                     |
| Código     | Controle de Fluxo                                                |       | Início E5 (Código)  |
|            |                                                                  | TBD   | Avaliação E4        |
| Código     | Controle de Fluxo                                                |       |                     |
| Execução   | Introdução, Registro de Ativação                                 |       |                     |
| Execução   | Chamada e retorno de Função, Passagem de parâmetros              |       | Início E6 (Chamada) |
| Otimização | Introdução, Janela e Grafos de Fluxo                             |       |                     |
|            |                                                                  | TBD   | Avaliação E5        |
| Otimização | Grafos de Fluxo e Redução de potência                            |       |                     |
|            | Revisão                                                          |       |                     |
|            | P2                                                               |       |                     |
|            | Considerações Finais e Fechamento                                |       |                     |
|            | PR                                                               |       |                     |

** Cronograma (para TEX)

#+name: cronograma
#+header: :var aulas=listagem_de_encontros
#+begin_src R :results output :session :exports both
suppressMessages(library(lubridate));
suppressMessages(library(tidyverse));
suppressMessages(library(xtable));

aulas <- aulas %>% as_tibble();

daysOff <- c(
# /1
seq(ymd("2020-03-05"), ymd("2020-03-15"), by="days"), # (Grenoble)
ymd("2020-04-07"), # (Seminário Geofísica)
ymd("2020-04-10"), # Religioso
ymd("2020-04-11"), # Não letivo
ymd("2020-04-15"), # (ERAD/RS)
ymd("2020-04-16"), # (ERAD/RS)
ymd("2020-04-17"), # (ERAD/RS)
ymd("2020-04-21"), # Tiradentes
ymd("2020-05-01"), # Trabalho
ymd("2020-05-16"), # Portas Abert
ymd("2020-06-11"), # Religioso
# /2
ymd("2020-09-07"), # Independência
seq(ymd("2020-09-14"), ymd("2020-09-18"), by="days"), # (SEMAC)
ymd("2020-09-20"), # Farroupilha
ymd("2020-10-12"), # Religioso
ymd("2020-10-28"), # Dia do Servidor Público
ymd("2020-11-02"), # Mortos
ymd("2020-11-15"), # República
)

dataDaRecuperação <- ymd("2020-07-14");

#dates <- tibble(Dia=seq(ymd("2020-08-06"), ymd("2020-12-23"), by="days")) %>% # 2020/2
dates <- tibble(Dia=seq(ymd("2020-03-05"), ymd("2020-07-15"), by="days")) %>% # 2020/1
    # Define o dia da semana
    mutate(DiaDaSemana = wday(Dia)) %>%
    # Aulas de Compiladores em quais dias da semana (segunda = 2, terca = 3, ...)
    filter(DiaDaSemana %in% c(3, 5)) %>%
    # Remove dias onde não haverá aula
    filter(!(Dia %in% daysOff));

aulas %>%
    # Associa as datas
    bind_cols(dates %>% slice(1:nrow(aulas))) %>%
    # Altera a data da PR
    mutate(Dia = case_when (grepl("PR", Descrição) ~ dataDaRecuperação,
                            TRUE ~ Dia)) %>%
    # Converte as datas para caracter
    mutate(Encontro = as.character(Dia)) %>%
    # Ordena as aulas
    mutate(N = 1:nrow(.)) -> aulas;

aulas %>%
    select(N, Encontro, Local, Setor, Descrição, Projeto) %>%
    as.data.frame() %>%
    xtable(.) %>%
    print (print.results=FALSE,
           booktabs = TRUE,
           include.rownames=FALSE) %>%
    as.character -> cronograma;
#+end_src

#+RESULTS: cronograma

#+begin_src R :results output :session :exports both
aulas %>%
    filter(!is.na(Local))
#+end_src

** Dias por etapa do projeto (em R)

#+name: dias_por_etapa
#+header: :var dep=cronograma
#+begin_src R :results output :session :exports both
aulas %>%
    select(Projeto, Dia) %>%
    filter(grepl("Início", Projeto)) %>%
    mutate(Projeto = gsub("Início ", "", Projeto)) %>%
    mutate(Projeto = gsub(" \\(.*$", "", Projeto)) %>%
    rename(Início = Dia) %>%
    left_join(
        aulas %>%
        select(Projeto, Dia) %>%
        filter(grepl("Avaliação", Projeto)) %>%
        mutate(Projeto = gsub("Avaliação ", "", Projeto)) %>%
        mutate(Projeto = strsplit(Projeto, ",")) %>%
        unnest(Projeto) %>%
        mutate(Projeto = trimws(Projeto)) %>%
        rename(Fim = Dia) %>%
        bind_rows(
            tribble(~Projeto, ~Fim,
                    "E6", (aulas %>%
                           filter(Descrição == "P2") %>%
                           pull(Encontro) %>%
                           as.Date)))
    ) %>%
    mutate(Duração = as.integer(Fim - Início)) %>%
    mutate(Texto = gsub("Início ", "", paste0(Projeto, " = ", Duração))) -> dias_por_etapa
#+end_src

#+RESULTS: dias_por_etapa
: 
: Joining, by = "Projeto"

** Cronograma (em PDF)

#+name: modelo_cronograma
#+header: :var dep0=cronograma
#+header: :var dep1=dias_por_etapa
#+begin_src R :results output :session :exports both
cronograma.modelo.filename = "data/be/43e1ff-0a91-4c3f-9e17-1fa62e6795ce/layout-cronograma.tex"
cronograma.modelo = readChar(cronograma.modelo.filename, file.info(cronograma.modelo.filename)$size);
turma = "A";
semestre = "2020/1 -- Outono";
cronograma.modelo <- gsub("TURMA", turma, cronograma.modelo);
cronograma.modelo <- gsub("SEMESTRE", semestre, cronograma.modelo);
cronograma.modelo <- gsub("TABELA", gsub("\\\\", "\\\\\\\\", cronograma), cronograma.modelo);
cronograma.modelo <- gsub("DIASPORETAPA", paste(dias_por_etapa$Texto, collapse=", "), cronograma.modelo);
write(cronograma.modelo, "cronograma.tex");
system2("rm", "-f cronograma.pdf")
system2("rubber", " --pdf cronograma.tex")
#+end_src

#+RESULTS: modelo_cronograma
: 
: compiling cronograma.tex...

* Cronograma                                                       :noexport:

O cronograma também está [[./cronograma.pdf][disponível em formato PDF]].

#+header: :var dep=cronograma
#+begin_src R :results value table :session :exports output :colnames yes
aulas %>%
    select(N, Encontro, Local, Setor, Descrição, Projeto) %>%
    as.data.frame
#+end_src

#+RESULTS:
|  N |   Encontro | Local | Setor      | Descrição                                                        | Projeto             |
|----+------------+-------+------------+------------------------------------------------------------------+---------------------|
|  1 | 2020-03-17 |       |            | Definições, requisitos e posicionamento da disciplina            | Definições iniciais |
|  2 | 2020-03-19 |       | Léxica     | Introdução, Expressões Regulares, =flex=                           | Início E1 (flex)    |
|  3 | 2020-03-24 |       | Léxica     | Autômato Finito Determinístico, Conv. AFND para AFD, =flex= (demo) |                     |
|  4 | 2020-03-26 |       | Sintática  | Intro., Gramáticas Livres de Contexto, Transformações            |                     |
|  5 | 2020-03-31 |       | Sintática  | Desc., com Retrocesso, Preditivo (Primeiro, Sequência)           |                     |
|  6 | 2020-04-02 |       | Sintática  | Parser e tabela LL(1), Exercício LL(1), =bison=                    | Início E2 (bison)   |
|  7 | 2020-04-09 |       | Sintática  | Ascendente, Parser LR(0), SLR(1) , =bison= (demo)                  |                     |
|  8 | 2020-04-14 |       | Sintática  | Exercícios LR(0), SLR(1)                                         |                     |
|  9 | 2020-04-23 |       | Sintática  | Parser LR(1), LALR(1)                                            |                     |
| 10 | 2020-04-28 | TBD   |            |                                                                  | Avaliação E1, E2    |
| 11 | 2020-04-30 |       | Sintática  | Exercícios LR(1), e LALR(1), AST e =bison= (ações)                 | Início E3 (Árvore)  |
| 12 | 2020-05-05 |       | Semântica  | Esquemas S e L-Atribuídos                                        |                     |
| 13 | 2020-05-07 |       | Semântica  | Implementação de Esquemas S e L-Atribuídos                       |                     |
| 14 | 2020-05-12 |       |            | Revisão                                                          | Início E4 (Tipos)   |
| 15 | 2020-05-14 |       |            | P1                                                               |                     |
| 16 | 2020-05-19 |       | Código     | Declarações, Escopos e Atribuição                                |                     |
| 17 | 2020-05-21 | TBD   |            |                                                                  | Avaliação E3        |
| 18 | 2020-05-26 |       | Código     | Expressões Lógicas com atalho, Controle de Fluxo                 |                     |
| 19 | 2020-05-28 |       | Código     | Endereçamento de Arranjos multidimensionais                      |                     |
| 20 | 2020-06-02 |       | Código     | Controle de Fluxo                                                | Início E5 (Código)  |
| 21 | 2020-06-04 | TBD   |            |                                                                  | Avaliação E4        |
| 22 | 2020-06-09 |       | Código     | Controle de Fluxo                                                |                     |
| 23 | 2020-06-16 |       | Execução   | Introdução, Registro de Ativação                                 |                     |
| 24 | 2020-06-18 |       | Execução   | Chamada e retorno de Função, Passagem de parâmetros              | Início E6 (Chamada) |
| 25 | 2020-06-23 |       | Otimização | Introdução, Janela e Grafos de Fluxo                             |                     |
| 26 | 2020-06-25 | TBD   |            |                                                                  | Avaliação E5        |
| 27 | 2020-06-30 |       | Otimização | Grafos de Fluxo e Redução de potência                            |                     |
| 28 | 2020-07-02 |       |            | Revisão                                                          |                     |
| 29 | 2020-07-07 |       |            | P2                                                               |                     |
| 30 | 2020-07-09 |       |            | Considerações Finais e Fechamento                                |                     |
| 31 | 2020-07-14 |       |            | PR                                                               |                     |




* Cronograma

Aguardando novo calendário acadêmico para 2020/1.
